package br.edu.vianna.dm.locacao.model;

import java.time.LocalDate;
import java.util.List;

public class Locacao {

	private Usuario usuario;
	private List<Filme> filmes;
	private LocalDate dataLocacao;
	private LocalDate dataRetorno;
	private Double valor;
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

    public LocalDate getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public LocalDate getDataRetorno() {
        return dataRetorno;
    }

    public void setDataRetorno(LocalDate dataRetorno) {
        this.dataRetorno = dataRetorno;
    }
	
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public List<Filme> getFilmes() {
		return filmes;
	}
	public void setFilmes(List<Filme> filmes) {
		this.filmes = filmes;
	}

    public boolean isAtrasado(LocalDate hoje) {        
        return dataRetorno.isAfter(hoje);
    }
	
}