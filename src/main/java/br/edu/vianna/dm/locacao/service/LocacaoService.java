package br.edu.vianna.dm.locacao.service;

import java.util.List;

import br.edu.vianna.dm.locacao.model.Filme;
import br.edu.vianna.dm.locacao.model.Locacao;
import br.edu.vianna.dm.locacao.model.Usuario;
import br.edu.vianna.dm.locacao.util.DataUtils;
import java.time.LocalDate;

public class LocacaoService {


    
    public Locacao alugarFilme(Usuario usuario, List<Filme> filmes) {
        
        Locacao locacao = new Locacao();
        locacao.setFilmes(filmes);
        locacao.setUsuario(usuario);
        locacao.setDataLocacao(LocalDate.now());
        Double valorTotal = 0d;
        for (int i = 0; i < filmes.size(); i++) {
            Filme filme = filmes.get(i);
            Double valorFilme = filme.getPrecoLocacao();
            switch (i) {
                case 2:
                    valorFilme = valorFilme * 0.75;
                    break;
                case 3:
                    valorFilme = valorFilme * 0.5;
                    break;
                case 4:
                    valorFilme = valorFilme * 0.25;
                    break;
                case 5:
                    valorFilme = 0d;
                    break;
            }
            valorTotal += valorFilme;
        }
        locacao.setValor(valorTotal);

        //Entrega no dia seguinte
        LocalDate dataEntrega = LocalDate.now().plusDays(1);
        if (DataUtils.ehDomingo(dataEntrega)) {
            dataEntrega = dataEntrega.plusDays(1);
        }
        locacao.setDataRetorno(dataEntrega);

        //Salvando a locacao...	
        //TODO adicionar método para salvar
        return locacao;
    }
    
}
